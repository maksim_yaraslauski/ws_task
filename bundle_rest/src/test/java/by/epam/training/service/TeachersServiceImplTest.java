package by.epam.training.service;

import by.epam.training.entities.Lesson;
import by.epam.training.entities.Teacher;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;

@DisplayName("Rest service test")
class TeachersServiceImplTest {
    private TeachersService teachersService;
    private Map<Long, Teacher> source;
    private static Teacher teacher_1;
    private static Teacher teacher_2;
    private static Teacher teacher_3;
    private static Teacher teacher_4;
    private static Teacher teacher_5;
    private static Teacher teacher_6;
    private static List<Teacher> teachers;

    @AfterAll
    static void afterAll() {
        teacher_1 = null;
        teacher_2 = null;
        teacher_3 = null;
        teacher_4 = null;
        teacher_5 = null;
        teacher_6 = null;
        teachers = null;
    }

    @AfterEach
    void afterEach() {
        teachersService = null;
        source = null;
    }

    @BeforeAll
    static void beforeAll() {
        List<Lesson> lessons_1 = new ArrayList<>();
        lessons_1.add(Lesson.ART);
        lessons_1.add(Lesson.ENGLISH);
        List<Lesson> lessons_2 = new ArrayList<>();
        lessons_2.add(Lesson.HANDWRITING);
        lessons_2.add(Lesson.PHYSICAL);
        List<Lesson> lessons_3 = new ArrayList<>();
        lessons_3.add(Lesson.HEALTH);
        lessons_3.add(Lesson.MUSIC);
        List<Lesson> lessons_4 = new ArrayList<>();
        lessons_4.add(Lesson.PHYSICAL);
        lessons_4.add(Lesson.SCIENCE);
        List<Lesson> lessons_5 = new ArrayList<>();
        lessons_5.add(Lesson.ART);
        lessons_5.add(Lesson.SCIENCE);
        List<Lesson> lessons_6 = new ArrayList<>();
        lessons_6.add(Lesson.PHYSICAL);
        lessons_6.add(Lesson.HANDWRITING);
        teachers = new ArrayList<>();
        teacher_1 = new Teacher(1L, "teacher_1", new Date(1000), lessons_1);
        teacher_2 = new Teacher(2L, "teacher_2", new Date(2000), lessons_2);
        teacher_3 = new Teacher(3L, "teacher_3", new Date(3000), lessons_3);
        teacher_4 = new Teacher(4L, "teacher_4", new Date(4000), lessons_4);
        teacher_5 = new Teacher(5L, "teacher_5", new Date(5000), lessons_5);
        teacher_6 = new Teacher(6L, "teacher_6", new Date(6000), lessons_6);
        teachers.add(teacher_1);
        teachers.add(teacher_2);
        teachers.add(teacher_3);
        teachers.add(teacher_4);
    }

    @BeforeEach
    void beforeEach() {
        source = mock(Map.class);
        when(source.get(1L)).thenReturn(teacher_1);
        when(source.get(2L)).thenReturn(teacher_2);
        when(source.get(3L)).thenReturn(teacher_3);
        when(source.get(4L)).thenReturn(teacher_4);
        when(source.put(5L, teacher_5)).thenReturn(teacher_5);
        when(source.put(6L, teacher_6)).thenReturn(teacher_6);
        when(source.values()).thenReturn(teachers);
        teachersService = new TeachersServiceImpl(source);
    }

    @Test
    @DisplayName("get all teachers test")
    void getTeachers() {
        assumeTrue(teachersService != null);
        assertEquals(teachersService.getTeachers(), teachers);
    }

    @Test
    @DisplayName("get teacher test")
    void getTeacher() {
        assumeTrue(teachersService != null);
        assertAll(
                () -> assertEquals(teacher_1, teachersService.getTeacher(1L)),
                () -> assertEquals(teacher_2, teachersService.getTeacher(2L)),
                () -> assertEquals(teacher_3, teachersService.getTeacher(3L)),
                () -> assertEquals(teacher_4, teachersService.getTeacher(4L))
        );
    }

    @Test
    @DisplayName("add teacher test")
    void addTeacher() {
        assumeTrue(teachersService != null);
        teachersService.addTeacher(teacher_5);
        when(source.get(5L)).thenReturn(teacher_5);
        assertEquals(teacher_5, teachersService.getTeacher(5L));
        teachersService.addTeacher(teacher_6);
        when(source.get(6L)).thenReturn(teacher_6);
        assertEquals(teacher_6, teachersService.getTeacher(6L));
    }

    @Test
    @DisplayName("update teacher test")
    void updateTeacher() {
        assumeTrue(teachersService != null);
        assertAll(
                () -> assertEquals(teacher_5, teachersService.updateTeacher(5L, teacher_5)),
                () -> assertEquals(teacher_6, teachersService.updateTeacher(6L, teacher_6))
        );
    }

    @ParameterizedTest
    @ValueSource(longs = {1L, 2L, 3L})
    @DisplayName("delete teacher test")
    void deleteTeacher(long id) {
        assumeTrue(teachersService != null);
        assertNull(teachersService.getTeacher(5L));
        teachersService.deleteTeacher(id);
        when(source.get(id)).thenReturn(null);
        assertNull(teachersService.getTeacher(id));
    }
}