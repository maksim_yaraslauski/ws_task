package by.epam.training.exceptions.rest;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class LessonNotFoundException extends Exception implements ExceptionMapper<LessonNotFoundException> {
    public LessonNotFoundException() {
        super("Lesson not found");
    }

    public LessonNotFoundException(String message) {
        super(message);
    }

    public Response toResponse(LessonNotFoundException exception) {
        return Response.status(404).entity(exception.getMessage())
                .type("application/json").build();
    }
}
