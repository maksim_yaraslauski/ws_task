package by.epam.training.exceptions.rest;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class TeacherNotFoundException extends Exception implements ExceptionMapper<TeacherNotFoundException> {
    public TeacherNotFoundException() {
        super("Teacher not found");
    }

    public TeacherNotFoundException(String message) {
        super(message);
    }

    public Response toResponse(TeacherNotFoundException exception) {

        return Response.status(404).entity(exception.getMessage())
                .type("application/json").build();
    }
}
