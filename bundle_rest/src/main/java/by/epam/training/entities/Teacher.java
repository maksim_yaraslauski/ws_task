package by.epam.training.entities;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@XmlRootElement(name="teacher")
@XmlAccessorType(XmlAccessType.FIELD)
public class Teacher {
    private Long id;
    private String name;
    private Date birthday;
    @XmlElement(name = "lesson", required = false)
    private List<Lesson> lessons;

    public Teacher() {
    }

    public Teacher(Long id, String name, Date birthday, List<Lesson> lessons) {
        this.id = id;
        this.name = name;
        this.birthday = birthday;
        this.lessons = lessons;
    }

    public Teacher(Long id, String name, Date birthday) {
        this.id = id;
        this.name = name;
        this.birthday = birthday;
        this.lessons = new ArrayList<Lesson>();
    }

    public Teacher addLesson(Lesson lesson) {
        this.lessons.add(lesson);
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Teacher teacher = (Teacher) o;

        if (!id.equals(teacher.id)) return false;
        if (!name.equals(teacher.name)) return false;
        return birthday.equals(teacher.birthday);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + birthday.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                ", lessons=" + lessons +
                '}';
    }
}
