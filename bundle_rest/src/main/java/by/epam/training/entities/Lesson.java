package by.epam.training.entities;

import javax.xml.bind.annotation.*;

@XmlType(name = "lesson")
@XmlEnum()
public enum Lesson {
    @XmlEnumValue("mathematics")
    MATH(1L, "Mathematics", 90L),
    @XmlEnumValue("english")
    ENGLISH(2L, "English", 90L),
    @XmlEnumValue("science")
    SCIENCE(3L, "Science", 90L),
    @XmlEnumValue("health")
    HEALTH(4L, "Health", 90L),
    @XmlEnumValue("handwriting")
    HANDWRITING(5L, "Handwriting", 90L),
    @XmlEnumValue("physical")
    PHYSICAL(6L, "Physical", 95L),
    @XmlEnumValue("art")
    ART(7L, "Art", 97L),
    @XmlEnumValue("music")
    MUSIC(8L, "Music", 90L);

    private Long id;
    private String name;
    private Long duration;

    Lesson(Long id, String name, Long duration) {
        this.id = id;
        this.name = name;
        this.duration = duration;
    }

    public String getNameInLowerCase() {
        return name.toLowerCase();
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "id=" + id +
                ", name='" +name + '\'' +
                ", duration=" + duration +
                '}';
    }
}
