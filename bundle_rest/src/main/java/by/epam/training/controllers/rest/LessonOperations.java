package by.epam.training.controllers.rest;

import by.epam.training.exceptions.rest.LessonNotFoundException;
import by.epam.training.exceptions.rest.TeacherNotFoundException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;

@Path("/teachers")
@Produces({"application/json"})
@Consumes({"application/xml"})
public interface LessonOperations {
    @PUT
    @Path("/addlesson/{id}/{lesson}")
    Response addLesson(@PathParam("id") Long id, @PathParam("lesson") String lesson) throws TeacherNotFoundException, LessonNotFoundException, URISyntaxException;
}
