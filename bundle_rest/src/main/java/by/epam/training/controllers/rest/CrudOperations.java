package by.epam.training.controllers.rest;

import by.epam.training.entities.Teacher;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;

@Path("/teachers")
@Produces({"application/json"})
@Consumes({"application/xml"})
public interface CrudOperations {
    @GET
    @Path("/")
    Response getTeachers();

    @GET
    @Path("/{id}")
    Response getTeacher(@PathParam("id") Long id);

    @POST
    @Path("/")
    Response postTeacher(Teacher teacher) throws URISyntaxException;

    @PUT
    @Path("/{id}")
    Response putTeacher(@PathParam("id") Long id, Teacher teacher)  throws URISyntaxException;

    @DELETE
    @Path("/{id}")
    Response deleteTeacher(@PathParam("id") Long id);
}
