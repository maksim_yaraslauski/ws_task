package by.epam.training.controllers.rest;

import by.epam.training.entities.Lesson;
import by.epam.training.entities.Teacher;
import by.epam.training.exceptions.rest.LessonNotFoundException;
import by.epam.training.exceptions.rest.TeacherNotFoundException;
import by.epam.training.service.TeachersService;

import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;


public class RestControllerImpl implements CrudOperations, LessonOperations {
    private TeachersService teachersService;

    public RestControllerImpl() {
    }

    public TeachersService getTeachersService() {
        return teachersService;
    }

    public void setTeachersService(TeachersService teachersService) {
        this.teachersService = teachersService;
    }

    public RestControllerImpl(TeachersService teachersService) {
        this.teachersService = teachersService;
    }

    public Response getTeachers() {
        return Response.ok(teachersService.getTeachers()).build();
    }

    public Response getTeacher(Long id) {
        return Response.ok(teachersService.getTeacher(id)).build();
    }

    public Response postTeacher(Teacher teacher) throws URISyntaxException {
        teachersService.addTeacher(teacher);
        return Response.created(new URI("rest/teachers/" + teacher.getId())).build();
    }

    public Response putTeacher(Long id, Teacher teacher) throws URISyntaxException {
        Teacher oldValue = teachersService.updateTeacher(id, teacher);
        if (oldValue == null) {
            return Response.created(new URI("rest/teachers/" + id)).build();
        }
        return Response.noContent().build();
    }

    public Response deleteTeacher(Long id) {
        teachersService.deleteTeacher(id);
        return Response.noContent().build();
    }

    public Response addLesson(Long id, String lesson) throws TeacherNotFoundException, LessonNotFoundException, URISyntaxException {
        Teacher teacher = teachersService.getTeacher(id);
        if (teacher == null) {
            throw new TeacherNotFoundException(String.format("Not found current teacher for id = %d", id));
        }
        Lesson newLesson;
        try {
            newLesson = Lesson.valueOf(lesson.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new LessonNotFoundException(String.format("Not found %s lesson", lesson));
        }
        teacher.addLesson(newLesson);
        return Response.created(new URI("rest/teachers/" + id)).build();
    }
}
