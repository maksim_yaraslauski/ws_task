package by.epam.training.service;

import by.epam.training.entities.Lesson;
import by.epam.training.entities.Teacher;

import java.util.*;

public class TeachersServiceImpl implements TeachersService {
    private static Map<Long, Teacher> teachers;
    static {
        teachers = new HashMap<Long, Teacher>();
        Teacher teacher_1 = new Teacher(1L, "Greg", new Date(100000));
        teacher_1.addLesson(Lesson.ART).addLesson(Lesson.HEALTH).addLesson(Lesson.ENGLISH);
        teachers.put(teacher_1.getId(), teacher_1);
        Teacher teacher_2 = new Teacher(2L, "Mike", new Date(200000));
        teacher_2.addLesson(Lesson.MATH).addLesson(Lesson.HANDWRITING).addLesson(Lesson.ENGLISH);
        teachers.put(teacher_2.getId(), teacher_2);
        Teacher teacher_3 = new Teacher(3L, "John", new Date(300000));
        teacher_3.addLesson(Lesson.PHYSICAL).addLesson(Lesson.ENGLISH).addLesson(Lesson.MUSIC);
        teachers.put(teacher_3.getId(), teacher_3);
    }

    public TeachersServiceImpl() {
    }

    public TeachersServiceImpl(Map source) {
        TeachersServiceImpl.teachers = source;
    }

    public List<Teacher> getTeachers() {
        return new ArrayList<Teacher>(teachers.values());
    }

    public Teacher getTeacher(Long id) {
        return teachers.get(id);
    }

    public void addTeacher(Teacher teacher) {
        teachers.put(teacher.getId(), teacher);
    }

    public Teacher updateTeacher(Long id, Teacher teacher) {
        return teachers.put(id, teacher);
    }

    public void deleteTeacher(Long id) {
        teachers.remove(id);
    }
}
