package by.epam.training.service;

import by.epam.training.entities.Teacher;

import java.util.List;

public interface TeachersService {
    Teacher getTeacher(Long id);
    List<Teacher> getTeachers();
    void addTeacher(Teacher teacher);
    Teacher updateTeacher(Long id, Teacher teacher);
    void deleteTeacher(Long id);
}
