package by.epam.training.controllers.soap;

import by.epam.training.exceptions.soap.WrongCurrencyException;
import net.webservicex.Currency;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

@DisplayName("Currency converter test")
class CurrencyConvertorSoapImplTest {
    private static CurrencyConvertorWithTotal currencyConvertor;

    @BeforeAll
    static void beforeAll() {
        currencyConvertor = new CurrencyConvertorSoapImpl();
    }

    @RepeatedTest(3)
    @DisplayName("repeated converter rate test")
    void repeatedConversionRate() {
        assumeTrue(currencyConvertor != null);
        assertEquals(-2.0, currencyConvertor.conversionRate(2.0, Currency.RUB, Currency.EUR));
    }

    @Test
    @DisplayName("converter well test")
    void conversionRateWithCorrectCurrency() {
        assumeTrue(currencyConvertor != null);
        assertAll(
                () -> assertEquals(-1.0, currencyConvertor.conversionRate(1.0, Currency.RUB, Currency.EUR)),
                () -> assertEquals(-2.0, currencyConvertor.conversionRate(2.0, Currency.RUB, Currency.USD)),
                () -> assertEquals(-3.0, currencyConvertor.conversionRate(3.0, Currency.RUB, Currency.RUB))
        );
        assertAll(
                () -> assertEquals(-1.0, currencyConvertor.conversionRate(1.0, Currency.USD, Currency.EUR)),
                () -> assertEquals(-2.0, currencyConvertor.conversionRate(2.0, Currency.USD, Currency.USD)),
                () -> assertEquals(-3.0, currencyConvertor.conversionRate(3.0, Currency.USD, Currency.RUB))
        );
        assertAll(
                () -> assertEquals(-1.0, currencyConvertor.conversionRate(1.0, Currency.EUR, Currency.EUR)),
                () -> assertEquals(-2.0, currencyConvertor.conversionRate(2.0, Currency.EUR, Currency.USD)),
                () -> assertEquals(-3.0, currencyConvertor.conversionRate(3.0, Currency.EUR, Currency.RUB))
        );
    }

    @ParameterizedTest
    @EnumSource(value = Currency.class, names = {"CVE", "SVC", "LVL"})
    @DisplayName("converter wrong test")
    void conversionRateWithWrongCurrency(Currency wrongCurrency) {
        assumeTrue(currencyConvertor != null);
        Throwable throwable = assertThrows(WrongCurrencyException.class, () ->
                currencyConvertor.conversionRate(2.0, wrongCurrency, Currency.RUB));
        assertEquals("Wrong currency. Expected [EUR, USD, RUB], but was "
                + String.format("[%s, %s]", wrongCurrency, "RUB"), throwable.getMessage());
    }
}