package by.epam.training.client.soap;

import by.epam.training.exceptions.soap.WrongCurrencyException;
import net.webservicex.Currency;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

@DisplayName("Soap client test")
class SoapClientTest {
    private static SoapClient soapClient;

    @BeforeAll
    static void beforeAll() {
        soapClient = new SoapClient();
    }

    @RepeatedTest(3)
    @DisplayName("repeated conversion rate test")
    void repeatedConversionRate() {
        assumeTrue(soapClient != null);
        assertEquals(-1.0, soapClient.conversionRate(Currency.RUB, Currency.EUR));
    }

    @Test
    @DisplayName("conversion rate well test")
    void conversionRateWithCorrectCurrency() {
        assumeTrue(soapClient != null);
        assertAll(
                () -> assertEquals(-1.0, soapClient.conversionRate(Currency.RUB, Currency.EUR)),
                () -> assertEquals(-1.0, soapClient.conversionRate(Currency.RUB, Currency.USD)),
                () -> assertEquals(-1.0, soapClient.conversionRate(Currency.RUB, Currency.RUB))
        );
        assertAll(
                () -> assertEquals(-1.0, soapClient.conversionRate(Currency.USD, Currency.EUR)),
                () -> assertEquals(-1.0, soapClient.conversionRate(Currency.USD, Currency.USD)),
                () -> assertEquals(-1.0, soapClient.conversionRate(Currency.USD, Currency.RUB))
        );
        assertAll(
                () -> assertEquals(-1.0, soapClient.conversionRate(Currency.EUR, Currency.EUR)),
                () -> assertEquals(-1.0, soapClient.conversionRate(Currency.EUR, Currency.USD)),
                () -> assertEquals(-1.0, soapClient.conversionRate(Currency.EUR, Currency.RUB))
        );
    }

    @ParameterizedTest
    @EnumSource(value = Currency.class, names = {"CVE", "SVC", "LVL"})
    @DisplayName("conversion rate wrong test")
    void conversionRateWithWrongCurrency(Currency wrongCurrency) {
        assumeTrue(soapClient != null);
        Throwable throwable = assertThrows(WrongCurrencyException.class, () ->
                soapClient.conversionRate(wrongCurrency, Currency.RUB));
        assertEquals("Wrong currency. Expected [EUR, USD, RUB], but was "
                + String.format("[%s, %s]", wrongCurrency, "RUB"), throwable.getMessage());
    }

    @ParameterizedTest
    @EnumSource(value = Currency.class, names = {"RUB", "EUR", "USD"})
    @DisplayName("correct currency test")
    void correctCurrency(Currency currency) {
        assumeTrue(soapClient != null);
        assertTrue(soapClient.isCorrect(currency));
    }

    @ParameterizedTest
    @EnumSource(value = Currency.class, names = {"CVE", "SVC", "LVL"})
    @DisplayName("wrong currency test")
    void wrongCurrency(Currency currency) {
        assumeTrue(soapClient != null);
        assertFalse(soapClient.isCorrect(currency));
    }
}