package by.epam.training.controllers.soap;


import by.epam.training.client.soap.SoapClient;
import net.webservicex.Currency;

public class CurrencyConvertorSoapImpl implements CurrencyConvertorWithTotal {
    public double conversionRate(double number, Currency fromCurrency, Currency toCurrency) {
        double totalAmoutn = new SoapClient().conversionRate(fromCurrency, toCurrency) * number;
        return totalAmoutn;
    }
}
