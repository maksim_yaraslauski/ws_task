package by.epam.training.controllers.soap;

import net.webservicex.ObjectFactory;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@WebService(targetNamespace = "http://www.webserviceX.NET/", name = "CurrencyConvertorWithTotal")
@XmlSeeAlso({ObjectFactory.class})
public interface CurrencyConvertorWithTotal {
    @WebMethod(operationName = "ConversionRate", action = "http://www.webserviceX.NET/ConversionRate")
    @RequestWrapper(localName = "ConversionRate", targetNamespace = "http://www.webserviceX.NET/", className = "by.epam.training.client.soap.entities.ConversionRate")
    @ResponseWrapper(localName = "ConversionRateResponse", targetNamespace = "http://www.webserviceX.NET/", className = "net.webservicex.ConversionRateResponse")
    @WebResult(name = "ConversionRateResult", targetNamespace = "http://www.webserviceX.NET/")
    public double conversionRate(
            @WebParam(name = "Number", targetNamespace = "http://www.webserviceX.NET/")
                    double number,
            @WebParam(name = "FromCurrency", targetNamespace = "http://www.webserviceX.NET/")
                    net.webservicex.Currency fromCurrency,
            @WebParam(name = "ToCurrency", targetNamespace = "http://www.webserviceX.NET/")
                    net.webservicex.Currency toCurrency
    );
}
