package by.epam.training.client.soap.entities;

import net.webservicex.Currency;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "number",
        "fromCurrency",
        "toCurrency"
})
@XmlRootElement(name = "ConversionRate")
public class ConversionRate {
    @XmlElement(name = "Number", required = true)
    @XmlSchemaType(name = "double")
    protected double number;
    @XmlElement(name = "FromCurrency", required = true)
    @XmlSchemaType(name = "string")
    protected Currency fromCurrency;
    @XmlElement(name = "ToCurrency", required = true)
    @XmlSchemaType(name = "string")
    protected Currency toCurrency;

    public double getNumber() {
        return number;
    }

    public void setNumber(double number) {
        this.number = number;
    }

    public Currency getFromCurrency() {
        return fromCurrency;
    }

    public void setFromCurrency(Currency fromCurrency) {
        this.fromCurrency = fromCurrency;
    }

    public Currency getToCurrency() {
        return toCurrency;
    }

    public void setToCurrency(Currency toCurrency) {
        this.toCurrency = toCurrency;
    }
}
