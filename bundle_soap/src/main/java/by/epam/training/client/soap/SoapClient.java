package by.epam.training.client.soap;

import by.epam.training.exceptions.soap.WrongCurrencyException;
import net.webservicex.Currency;
import net.webservicex.CurrencyConvertor;
import net.webservicex.CurrencyConvertorSoap;

import java.util.HashSet;
import java.util.Set;

public class SoapClient {
    private static Set<Currency> currencies;
    static  {
        currencies = new HashSet<Currency>();
        currencies.add(Currency.USD);
        currencies.add(Currency.EUR);
        currencies.add(Currency.RUB);
    }

    public double conversionRate(Currency fromCurrency, Currency toCurrency) {
        if (isCorrect(fromCurrency) && isCorrect(toCurrency)) {
            CurrencyConvertor service = new CurrencyConvertor();
            CurrencyConvertorSoap port = service.getCurrencyConvertorSoap();
            return port.conversionRate(fromCurrency, toCurrency);
        } else {
            throw new WrongCurrencyException("Wrong currency. Expected [EUR, USD, RUB], but was "
                    + String.format("[%s, %s]", fromCurrency, toCurrency));
        }
    }

    protected boolean isCorrect(Currency currency) {
        return currencies.contains(currency);
    }

}
