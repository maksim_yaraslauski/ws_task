package by.epam.training.exceptions.soap;

import net.webservicex.Currency;

import javax.xml.ws.WebFault;

@WebFault(name = "WrongCurrencyException", targetNamespace = "http://www.webserviceX.NET/")
public class WrongCurrencyException extends RuntimeException {
    private Currency faultInfo;

    public WrongCurrencyException(String message) {
        super(message);
    }
    public WrongCurrencyException(Currency currency) {
        super("Wrong currency: " + currency.name());
    }

    public WrongCurrencyException(Currency currency, Currency faultInfo) {
        super("Wrong currency:");
        this.faultInfo = faultInfo;
    }

    public WrongCurrencyException(Currency currency, Currency faultInfo, Throwable cause) {
        super("Wrong currency:", cause);
        this.faultInfo = faultInfo;
    }

    public Currency getFaultInfo() {
        return faultInfo;
    }
}
