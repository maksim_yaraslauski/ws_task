1. feature:repo-add mvn:by.training.epam/feature_module/1.0/xml/features
2. feature:install kar_service
3. bundle:list
4. Send requests like this on http://localhost:8181/cxf/soap:
	<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://www.webserviceX.NET/">
   		<soapenv:Header/>
   		<soapenv:Body>
      		<web:ConversionRate>
     	 		<Number>2.2</Number>
         		<FromCurrency>RUB</FromCurrency>
         		<ToCurrency>EUR</ToCurrency>
     		 </web:ConversionRate>
   		</soapenv:Body>
	</soapenv:Envelope>
5. feature:uninstall kar_service
6. feature:repo-remove kar_service
